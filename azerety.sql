insert into utilisateur (id_utilisateur, nom, prenom, adresse, age , poids, mdp)
values ('bill22','azert' , 'bill','rue du groin' , '24', '83', 'password'),
('bill35','azert' , 'bill',' rue de callac' , '36', '70', 'pass'),
('bill56','azerty' , 'billy',' rue du lac' , '45', '75', 'pd'),
('bill29','azertyu' , 'bob','  rue du bourg' , '65', '78', 'psd');

insert into repas (id_repas, caloriesAugm, nombreRepas, typeDeRepas, dateDeDebut, dateDeFin, id_utilisateur)
values ('1' , '400',' 1',' kebab', '2019-06-01',' 2019-06-01',' bill22'),
('2' ,' 350',' 1',' burger',' 2019-06-02',' 2019-06-02',' bill35'),
('3 ',' 120',' 1',' salade',' 2019-06-03', '2019-06-03', 'bill56'),
('4' ,' 180',' 1',' soupe',' 2019-06-04', '2019-06-04',' bill29');

insert into depense (sport, id_calories)
values ('velo',' 500'),
('foot', '600'),
('basket',' 450'),
('tennis',' 550');

insert into calories (id_calories, caloriesDep, caloriesAugm)
values ('1', '500','400'),
('2','600','350'),
('3','450','120'),
('4','550','180');


