package application;
 
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.image.Image; 

public class MenuPrincipal  { 
	public static Stage stage;

   public MenuPrincipal() {      
       
      Text text1 = new Text("Date");                     
      Text text2 = new Text("Calories consomm�s");                    
      Text text3 = new Text("Calories brul�es");

      TextField textField1 = new TextField();                           
      TextField textField2 = new TextField(); 
      TextField textField3 = new TextField();

      Button b1mp = new Button("Menu principal");
      Button b2mp = new Button("Pr�f�rences");
      Button b3mp = new Button("Choix sport et repas");
      Button b4mp = new Button("Suivi d'activit�s");
      Button b5mp = new Button("Inscription");
      Button b6mp = new Button("Conditions d'utilisation");           
 
      BorderPane root = new BorderPane();
      
      GridPane gridpane = new GridPane();
      
      HBox tophbox = new HBox(b1mp,b2mp,b3mp,b4mp,b5mp,b6mp);
      tophbox.setAlignment(Pos.TOP_CENTER);
      
      HBox bothbox = new HBox();
      bothbox.setAlignment(Pos.BOTTOM_CENTER);
      bothbox.getChildren().add(gridpane);
              
      BackgroundImage myBI= new BackgroundImage(new Image("file:fondEcran.png",550,550,false,true), 
 		     BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, 
 		      BackgroundSize.DEFAULT);      
        
      final PieChart chart = new PieChart(); 
      chart.setTitle("Graphique calories"); 
      chart.getData().setAll(new PieChart.Data("Calories brul�es", 50), new PieChart.Data("Calories consomm�es", 30));  
      chart.setMaxSize(50, 50);
      
//        gridPane.setMinSize(500, 500); 
//        gridPane.setPadding(new Insets(10, 10, 10, 10));    
//        gridPane.setVgap(5); 
//        gridPane.setHgap(5);                 
        gridpane.add(text1, 2, 1); 
        gridpane.add(textField1, 1, 1); 
        gridpane.add(text2, 2, 2);       
        gridpane.add(textField2, 1, 2);
        gridpane.add(text3, 2, 3); 
        gridpane.add(textField3, 1, 3);
        gridpane.add(chart,0,5);
        root.setBackground(new Background(myBI));
        root.setTop(tophbox);
        root.setCenter(gridpane);
            
      stage.setResizable(false);  
      stage.setTitle("Inscription");                
      stage.setScene(new Scene(root, 500,500));     
      stage.show();      
   } }


