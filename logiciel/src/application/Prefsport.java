package application;

import javafx.application.Application; 
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage; 

public class Prefsport extends Application { 

@Override 
   public void start(Stage stage) {      
       
      Button b1 = new Button("Menu principal");
      Button b2 = new Button("Préférences");
      Button b3 = new Button("Choix sport et repas");
      Button b4 = new Button("Suivi d'activités");
      Button b5 = new Button("Inscription");
      Button b6 = new Button("Conditions d'utilisation");
      Button b7 = new Button("modifier");
      Button b8 = new Button("valider");
      Button b9 = new Button("modifier");
      Button b10 = new Button("valider");
      Button b11 = new Button("modifier");
      Button b12 = new Button("valider");
      Button b13 = new Button("modifier");
      Button b14 = new Button("valider");
      Button b15 = new Button("valider");
      Button b16 = new Button("modifier");
      Button b17 = new Button("valider");
      Button b18 = new Button("modifier");
      Button b19 = new Button("valider");
      Button b20 = new Button("modifier");
      Button b21 = new Button("valider");
      Button b22 = new Button("modifier");
      
    Text text1 = new Text("Pseudo");                    
    Text text2 = new Text("Nom"); 
    Text text3 = new Text("Prenom");                    
    Text text4 = new Text("Adresse");
      
    TextField textField1 = new TextField();                           
    TextField textField2 = new TextField(); 
    TextField textField3 = new TextField();                           
    TextField textField4 = new TextField();
 
      BorderPane root = new BorderPane();
      
      GridPane gridpane = new GridPane();
      GridPane gridpane2 = new GridPane();
      GridPane gridpane3 = new GridPane();
      
     gridpane.setAlignment(Pos.CENTER);
     gridpane2.setAlignment(Pos.CENTER);
     gridpane3.setAlignment(Pos.CENTER);
     gridpane.add(text1, 0, 0); 
     gridpane.add(textField1, 1, 0); 
     gridpane.add(text2, 0, 1);       
     gridpane.add(textField2, 1, 1);
     gridpane.add(text3, 0, 2); 
     gridpane.add(textField3, 1, 2); 
     gridpane.add(text4, 0, 3);       
     gridpane.add(textField4, 1, 3);
     gridpane2.add(b7, 0, 0);       
     gridpane2.add(b8, 1, 0);
     gridpane2.add(b9, 0, 1);       
     gridpane2.add(b10, 1, 1);
     gridpane2.add(b11, 0, 2);       
     gridpane2.add(b12, 1, 2);
     gridpane2.add(b13, 0, 3);       
     gridpane2.add(b14, 1, 3);
     gridpane3.add(b15, 0, 0);       
     gridpane3.add(b16, 1, 0);
     gridpane3.add(b17, 0, 1);       
     gridpane3.add(b18, 1, 1);
     gridpane3.add(b19, 0, 2);       
     gridpane3.add(b20, 1, 2);
     gridpane3.add(b21, 0, 3);       
     gridpane3.add(b22, 1, 3);
      
      HBox tophbox = new HBox(b1,b2,b3,b4,b5,b6);
      HBox centhbox= new HBox(gridpane);
      HBox righbox= new HBox(gridpane2);
      HBox lefthbox= new HBox(gridpane3);
      
      centhbox.setBackground(Background.EMPTY);
              
      BackgroundImage myBI= new BackgroundImage(new Image("file:fondEcran.png",550,550,false,true), 
 		     BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, 
 		      BackgroundSize.DEFAULT);
      
        root.setBackground(new Background(myBI));
        root.setTop(tophbox);
        root.setLeft(lefthbox);
        root.setCenter(centhbox);
        root.setRight(righbox);
    
      stage.setResizable(false);  
      stage.setTitle("Inscription");                
      stage.setScene(new Scene(root, 500,500));     
      stage.show();      
   } 
	    
public static void main(String[] args) { launch(args); }
}

