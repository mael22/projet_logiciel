package application;

import javafx.application.Application;
import javafx.geometry.Insets; 
import javafx.geometry.Pos; 
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage; 

public class test extends Application { 

@Override 
   public void start(Stage stage) {      
       
      Text text1 = new Text("Date");                     
      Text text2 = new Text("Calories consomm�s");                    
      Text text3 = new Text("Calories brul�es");

      TextField textField1 = new TextField();                           
      TextField textField2 = new TextField(); 
      TextField textField3 = new TextField();

      Button b1 = new Button("Menu principal");
      Button b2 = new Button("Pr�f�rences");
      Button b3 = new Button("Choix sport et repas");
      Button b4 = new Button("Suivi d'activit�s");
      Button b5 = new Button("Inscription");
      Button b6 = new Button("Conditions d'utilisation");           
 
      GridPane gridPane = new GridPane(); 
      
      Scene scene = new Scene(gridPane);
      HBox hbox = new HBox();
      hbox.getChildren().addAll(b1,b2,b3,b4,b5,b6);
     // hbox.setAlignment(Pos.TOP_CENTER);
              
      BackgroundImage myBI= new BackgroundImage(new Image("file:fondEcran.png",550,550,false,true), 
 		     BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, 
 		      BackgroundSize.DEFAULT);      
        
      final PieChart chart = new PieChart(); 
      chart.setTitle("Graphique calories"); 
      chart.getData().setAll(new PieChart.Data("Calories brul�es", 50), new PieChart.Data("Calories consomm�es", 30));  
      chart.setMaxSize(50, 50);
      
      gridPane.setMinSize(500, 500); 
      gridPane.setPadding(new Insets(10, 10, 10, 10));    
      gridPane.setVgap(5); 
      gridPane.setHgap(5);                 
      gridPane.add(text1, 2, 1); 
      gridPane.add(textField1, 1, 1); 
      gridPane.add(text2, 2, 2);       
      gridPane.add(textField2, 1, 2);
      gridPane.add(text3, 2, 3); 
      gridPane.add(textField3, 1, 3);
      gridPane.setBackground(new Background(myBI));
      gridPane.add(chart,0,5);
     // gridPane.add(b1,0,0);
     // gridPane.add(b2,1,0);
      //gridPane.add(b3,2,0);
     // gridPane.add(b4,3,0);
     // gridPane.add(b5,4,0);
     // gridPane.add(b6,5,0);
     
      stage.setResizable(false);  
      stage.setTitle("Inscription");                
      stage.setScene(scene);     
      stage.show();      
   } 

