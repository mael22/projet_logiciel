package application;

import java.time.LocalDate;
import jfxtras.scene.control.CalendarPicker;
import javafx.application.Application;
import javafx.geometry.Insets; 
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Menu;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage; 

public class Suiviact  { 
	
	
	Scene suiviact;

   public Suiviact() {      
       
      Button b1sa = new Button("Menu principal");
      Button b2sa = new Button("Préférences");
      Button b3sa = new Button("Choix sport et repas");
      Button b4sa = new Button("Suivi d'activités");
      Button b5sa = new Button("Inscription");
      Button b6sa = new Button("Conditions d'utilisation");           
 
      BorderPane root = new BorderPane();
      
      GridPane gridpane = new GridPane();
      suiviact = new Scene(gridpane); 
      
      HBox tophbox = new HBox(b1sa,b2sa,b3sa,b4sa,b5sa,b6sa);

      HBox righbox= new HBox ();
      righbox.setBackground(Background.EMPTY);
              
      BackgroundImage myBI= new BackgroundImage(new Image("file:fondEcran.png",550,550,false,true), 
 		     BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, 
 		      BackgroundSize.DEFAULT);
      
        CalendarPicker cal = new CalendarPicker();
 
        root.setBackground(new Background(myBI));
        root.setTop(tophbox);
        root.setRight(cal);
       
   } 

		public Scene getScene() {
			return this.suiviact;
		}
		
		public Scene setScene(Scene e) {
			return this.suiviact = e;
		}}
