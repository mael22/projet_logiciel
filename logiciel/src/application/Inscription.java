package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Inscription {

	GridPane gridPane = new GridPane();
	Scene sceneInscription;

	TextField textField1 = new TextField();
	TextField textField2 = new TextField();
	TextField textField3 = new TextField();
	TextField textField4 = new TextField();
	TextField textField5 = new TextField();
	TextField textField6 = new TextField();
	TextField textField7 = new TextField();
	
	public Inscription() {
		
		Text text1 = new Text("Pseudo");
		Text text2 = new Text("Nom");
		Text text3 = new Text("Prenom");
		Text text4 = new Text("Email");
		Text text5 = new Text("age");
		Text text6 = new Text("poids");
		Text text7 = new Text("mdp");

		Button b1 = new Button("valider");
		Button b2 = new Button("annuler");
		// button.setCancelButton(true);
		gridPane.setMinSize(500, 500);
		gridPane.setPadding(new Insets(10, 10, 10, 10));
		gridPane.setVgap(5);
		gridPane.setHgap(5);
		gridPane.setAlignment(Pos.CENTER);
		gridPane.add(text1, 0, 0);
		gridPane.add(textField1, 1, 0);
		gridPane.add(text2, 0, 1);
		gridPane.add(textField2, 1, 1);
		gridPane.add(text3, 0, 2);
		gridPane.add(textField3, 1, 2);
		gridPane.add(text4, 0, 3);
		gridPane.add(textField4, 1, 3);
		gridPane.add(text7, 0, 4);
		gridPane.add(textField7, 1, 4);
		gridPane.add(b1, 1, 5);
		gridPane.add(b2, 2, 5);

		BackgroundImage myBI = new BackgroundImage(new Image("file:fondEcran.png", 550, 550, false, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);

		gridPane.setBackground(new Background(myBI));
		sceneInscription = new Scene(gridPane);

		b2.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {

				PageAccueil.stage.setScene(PageAccueil.getscene());
			}
		});
		b1.setOnAction(new EventHandler<ActionEvent>() {  /*bouton valider pour rentrer les donnees en base de donn�es*/

			public void handle(ActionEvent event) {

				String pseudo = textField1.getText();
				String nom = textField2.getText();
				String prenom = textField3.getText();
				String adresse = textField4.getText();
				String mdp = textField5.getText();
				
				try {
					PreparedStatement pstm = PageAccueil.cnx.prepareStatement("INSERT INTO utilisateur (id_utilisateur,"
							+ " nom, prenom, adresse, mdp)"
					          + " VALUES (?,?,?,?,?)"); /*nb de caractere*/
					pstm.setString(1, pseudo);
					pstm.setString(2, nom);
					pstm.setString(3, prenom);
					pstm.setString(4, adresse);
					pstm.setString(5, mdp); 
					
					
					int inserted = pstm.executeUpdate();
					  
				} catch (SQLException e) {
			
					e.printStackTrace();
				}
				
				//PageAccueil.stage.setScene(PageAccueil.getscene());
			}
		});
	}

	public Scene getScene() {
		return this.sceneInscription;
	}

	public Scene setScene(Scene a) {
		return this.sceneInscription = a;
	}
}
